CXX = gcc
TARGET = exec
ARCHIVE = LO44_CabezasRemi

SOURCEO = propositions regles moteur_inference
SOURCE = $(SOURCEO) main

SOURCESFILES = $(SOURCE:=.c)
HEADERFILES = $(SOURCEO:=.h)
OBJETCSFILES = $(SOURCE:=.o)

CCFLAGS = -Wall -Werror -pedantic -ansi


launch : $(TARGET)
	@echo	"\n ... Système expert ..."
	./$(TARGET)
	@echo "\n"

$(TARGET) : $(OBJETCSFILES)
	@echo	"\n ... linking ..."
	$(CXX) $(CCFLAGS) -o $(TARGET) $(OBJETCSFILES)

.c.o :
	@echo 	"\n ... compilation of" $@ "..."
	$(CXX) $(CCFLAGS) -c -o $@ $<

clean :
	@echo	"\n ... clean ..."
	rm -rf *.o *~ $(TARGET)

zip :
	@echo "\n... Compression ..."
	mkdir $(ARCHIVE)
	cp Makefile $(SOURCESFILES) $(HEADERFILES) $(ARCHIVE)/
	cp $(ARCHIVE)_rapport.pdf propositions.txt regles.txt $(ARCHIVE)/
	zip -r $(ARCHIVE).zip $(ARCHIVE)/
	rm -r $(ARCHIVE)
