/**
	LO44
	Rémi Cabezas
	Fichier source contenant les sous-programmes de gestion des règles.
**/

#include "regles.h"

regle creerRegle(l_prop prem, l_prop ccl)
{
	regle r;
	r.premisse = prem;
	r.conclusion = ccl;
	return r;
}

int rvide(regle Regle)
{
	return lvide(Regle.premisse);
}

l_regle rAjouterQ(l_regle lsRegle, regle r)
{
	/*printf("rAjoutQ\n");*/
	l_regle p;
	l_regle temp;
	temp = (l_regle)malloc(sizeof(regle));
	temp->valeur=r;
	temp->suivant = NULL;

	if(lsRegle == NULL)
	{
		lsRegle = temp;
	}
	else
	{
		p = lsRegle;
		while(p->suivant != NULL)
		{
			p = p->suivant;
		}
		p->suivant = temp;
	}
	/*printf("end rAjouterQ\n");*/
	return lsRegle;
}

void rAfficher(l_regle lsRegle)
{
	/*printf("rAfficher\n");*/
	regle r = lsRegle->valeur;
	l_prop prem = r.premisse;
	while(prem != NULL)
	{
		printf("%s ", prem->valeur);
		prem = prem->suivant;
		if (prem != NULL)
		{
			printf("+ ");
		}
	}
	printf(" = %s", r.conclusion->valeur);
	/*printf("end rAfficher\n");*/
}

void lrAfficher(l_regle lsRegle)
{
	/*printf("lrAfficher\n");*/
	int i = 1;
	l_regle p = lsRegle;
	while(p != NULL)
	{
		printf("%d) ", i);
		rAfficher(p);
		printf("\n");
		p = p->suivant;
		i++;
	}
	/*printf("end lrAfficher\n");*/
}

l_regle regleParNum(l_regle lsRegle, int num)
{
	/*printf("regleParNum\n");*/
	int i=1;
	l_regle p = lsRegle;
	if(p != NULL && num != 0)
	{
		while(p->suivant != NULL && i < num)
		{
			i++;
			p = p->suivant;
		}
		/*printf("end if regleParNum\n");*/
		return p;
	}
	/*printf("end else regleParNum\n");*/
	return NULL;
}

l_regle copieLRegle(l_regle lsRegle)
{
	/*printf("copieLRegle\n");*/
	l_regle lsRegleRes = NULL;
	l_regle p = lsRegle;
	l_prop prem = NULL;
	l_prop ccl = NULL;

	while(p != NULL)
	{
		prem = copieLProp(p->valeur.premisse);
		ccl = copieLProp(p->valeur.conclusion);
		lsRegleRes = rAjouterQ(lsRegleRes, creerRegle(prem, ccl));
		p = p->suivant;
	}
	/*printf("end if copieLRegle\n");*/
	return lsRegleRes;
}

l_regle rSupprimerT(l_regle lsRegle)
{
	/*printf("rSupprimerT\n");*/
	if(lsRegle != NULL)
	{
    supprimerLProp(lsRegle->valeur.premisse);
    supprimerLProp(lsRegle->valeur.conclusion);
		free(lsRegle);
		lsRegle = lsRegle->suivant;
	}
	/*printf("end rSupprimerT\n");*/
	return lsRegle;
}

l_regle rSupprimerNum(l_regle lsRegle, int num)
{
	/*printf("rSupprimerNum\n");*/
	if(lsRegle != NULL)
	{
		if(num == 1)
		{
			lsRegle = rSupprimerT(lsRegle);
		}
		else
		{
			l_regle p = NULL;
			p = regleParNum(lsRegle, num-1);
			supprimerLProp(p->suivant->valeur.premisse);
			supprimerLProp(p->suivant->valeur.conclusion);
			free(p->suivant);
			if(p->suivant != NULL)
			{
				p->suivant = p->suivant->suivant;
			}
			else
			{
				p->suivant = NULL;
			}
		}
	}
	/*printf("end rSupprimerNum\n");*/
	return lsRegle;
}

void supprimerLRegle(l_regle lsRegle)
{
	/*printf("supprimerLRegle\n");*/
	while( lsRegle != NULL)
	{
		lsRegle = rSupprimerT(lsRegle);
	}
	/*printf("end supprimerLRegle\n");*/
}

void ecrireRegles(l_regle lsRegle, char* nFichier)
{
	FILE* fichier = NULL;
	l_regle p = lsRegle;
	l_prop q = NULL;

  fichier = fopen(nFichier, "w");

  if (fichier != NULL)
  {
		while(p != NULL)
		{
			q = p->valeur.premisse;
			while(!lvide(q))
			{
	       fputs(q->valeur, fichier);
				fputs("\n", fichier);
				q = q->suivant;
			}
			fputs(";\n", fichier);
			fputs(p->valeur.conclusion->valeur, fichier);
			fputs("\n", fichier);
			p = p->suivant;
		}
    fclose(fichier);
  }
}

l_regle lireRegles(char* nFichier)
{
	FILE* fichier = NULL;
	char chaine[MAX_SIZE + 2] = "";
	char * dernierCar = NULL;
	l_regle lsRegle = NULL;
	l_prop prem = NULL;
	l_prop ccl = NULL;

  fichier = fopen(nFichier, "r");

  if (fichier != NULL)
  {
		while (fgets(chaine, sizeof(chaine), fichier) != NULL)
		{
			dernierCar = strchr(chaine, '\n');
			*dernierCar = '\0';
			if(strcmp(chaine, ";") != 0)
			{
				prem = pAjouterQ(prem, chaine);
			}
			else
			{
				if (fgets(chaine, sizeof(chaine), fichier) != NULL)
				{
					dernierCar = strchr(chaine, '\n');
					*dernierCar = '\0';
					ccl = pAjouterQ(ccl, chaine);
					lsRegle = rAjouterQ(lsRegle, creerRegle(prem, ccl));
					prem = NULL;
					ccl = NULL;
				}
			}
		}
		fclose(fichier);
  }
	else
	{
		printf("Le fichier n'existe pas!\n");
	}
	return lsRegle;
}
