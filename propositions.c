/**
	LO44
	Rémi Cabezas
	Fichier source contenant les sous-programmes de gestion des popositions.
**/

#include "propositions.h"


l_prop pAjouterT(l_prop lsProp, char* libProp)
{
	/*printf("ajoutT\n");*/
	l_prop temp;
	temp = (l_prop)malloc(sizeof(proposition));
	temp->valeur=malloc(sizeof(strlen(libProp + 1)));
	strcpy(temp->valeur, libProp);
	temp->suivant = lsProp;

	return temp;
}

l_prop pAjouterQ(l_prop lsProp, char* libProp)
{
	/*printf("ajoutQ\n");*/
	l_prop p;
	l_prop temp;
	temp = (l_prop)malloc(sizeof(proposition));
	temp->valeur=malloc(sizeof(strlen(libProp + 1)));
	strcpy(temp->valeur, libProp);
	temp->suivant = NULL;

	if(lvide(lsProp))
	{
		lsProp = temp;
	}
	else
	{
		p = lsProp;
		while(!lvide(p->suivant))
		{
			p = p->suivant;
		}
		p->suivant = temp;
	}
	/*printf("end ajouterQ\n");*/
	return lsProp;
}

int lvide(l_prop lsProp)
{
	return lsProp == NULL;
}

char* lTete(l_prop lsProp)
{
	return lsProp->valeur;
}

void pAfficher(l_prop lsProp)
{
	printf("%s", lTete(lsProp));
}

void lpAfficher(l_prop lsProp)
{
	/*printf("lpAfficher\n");*/
	int i = 1;
	l_prop p = lsProp;

	while(p != NULL)
	{
		printf("%d) ", i);
		pAfficher(p);
		printf("\n");
		i++;
		p = p->suivant;
	}
	/*printf("end lpAfficher\n");*/
}

l_prop  copieLProp(l_prop lsProp)
{
	/*printf("copieLProp\n");*/
	l_prop lsPropRes = NULL;
	l_prop p = lsProp;
	char* texte = NULL;


	while(!lvide(p))
	{
		texte = malloc(sizeof(strlen(p->valeur + 1)));
		strcpy(texte, p->valeur);
		lsPropRes = pAjouterQ(lsPropRes, texte);
		p = p->suivant;
	}
	/*printf("end if copieLProp\n");*/
	return lsPropRes;
}

int nbProp(l_prop lsProp)
{
	l_prop p = lsProp;
	int i = 0;
	while (!lvide(p))
	{
		i++;
		p = p->suivant;
	}
	return i;
}

l_prop propParNum(l_prop lsProp, int num)
{
	/*printf("propParNum\n");*/
	int i=1;
	l_prop p = lsProp;
	if(!lvide(p) && num != 0)
	{
		while(p->suivant != NULL && i < num)
		{
			i++;
			p = p->suivant;
		}
		/*printf("end if propParNum\n");*/
		return p;
	}
	/*printf("end else propParNum\n");*/
	return NULL;
}

l_prop supprimerT(l_prop lsProp)
{
	/*printf("supprimerT\n");*/
	if(lsProp != NULL)
	{
		free(lsProp);
		lsProp = lsProp->suivant;
	}
	/*printf("end supprimerT\n");*/
	return lsProp;
}

l_prop supprimerNum(l_prop lsProp, int num)
{
	/*printf("supprimerNum\n");*/
	if(!lvide(lsProp))
	{
		if(num == 1)
		{
			lsProp = supprimerT(lsProp);
		}
		else
		{
			l_prop p = lsProp;
			p = propParNum(lsProp, num-1);
			free(p->suivant);
			if(p->suivant != NULL)
			{
				p->suivant = p->suivant->suivant;
			}
			else
			{
				p->suivant = NULL;
			}
		}
	}
	/*printf("end supprimerNum\n");*/
	return lsProp;
}

l_prop supprimerQ(l_prop lsProp)
{
	l_prop p = lsProp;
	if (p != NULL)
	{
		if (p->suivant != NULL)
		{
			while(p->suivant->suivant != NULL)
			{
				p = p->suivant;
			}
			free(p->suivant);
			p->suivant = NULL;
		}
		else
		{
			free(p);
			p = NULL;
		}
	}
	return lsProp;
}

void supprimerLProp(l_prop lsProp)
{
	/*printf("supprimerLProp\n");*/
	while(!lvide(lsProp))
	{
		lsProp = supprimerT(lsProp);
	}
	/*printf("end supprimerLProp\n");*/
}

void ecrirePropositions(l_prop lsProp, char* nFichier)
{
	FILE* fichier = NULL;
	l_prop p = lsProp;
  fichier = fopen(nFichier, "w");

  if (fichier != NULL)
  {
		while(!lvide(p))
		{
      fputs(p->valeur, fichier);
			fputs("\n", fichier);
			p = p->suivant;
		}
    fclose(fichier);
  }
}

l_prop lirePropositions(char* nFichier)
{
	FILE* fichier = NULL;
	char chaine[41] = "";
	char * dernierCar = NULL;
	l_prop lsProp = NULL;

  fichier = fopen(nFichier, "r");

  if (fichier != NULL)
  {
		while (fgets(chaine, sizeof(chaine), fichier) != NULL)
		{
			dernierCar = strchr(chaine, '\n');
			*dernierCar = '\0';
			lsProp = pAjouterQ(lsProp, chaine);
		}
		fclose(fichier);
  }
	return lsProp;
}
