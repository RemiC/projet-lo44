/**
	LO44
	Rémi Cabezas
	Fichier d'en-têtes des sous-programmes de gestion des règles.
**/

#include "propositions.h"

typedef struct{
	l_prop premisse;
	l_prop conclusion;
}regle;

typedef struct lreg{
	regle valeur;
	struct lreg *suivant;
}regles;

typedef regles *l_regle;

/*
	Entrée:
		-prem: liste de propositions.
		-ccl liste de propositions.
	But:
		Créer une nouvelle règle ayant pour prémisse la liste prem et pour conclusion la liste de propositions ccl.
	Sortie:
		Nouvelle règle.
*/
regle creerRegle(l_prop prem, l_prop ccl);

/**
	Entrée:
		-Regle: une règle.
	But:
		Tester si la premisse d'une règle est vide.
	Sortie:
		- 1 si la premisse est vide.
		- 0 sinon.
**/
int rvide(regle Regle);

/*
	Entrée:
		-lsRegle: liste de règles.
		-r: une règle.
	But:
		Ajouter en fin de lsRegle la règle r.
	Sortie:
		lsRegle augmentée de r en fin.
*/
l_regle rAjouterQ(l_regle lsRegle, regle r);

/*
	Entrée:
		-lsregle: liste de règles.
	But:
		Afficher la règle en tête de lsRegle.
	Sortie:
		Aucune.
*/
void rAfficher(l_regle lsRegle);

/*
	Entrée:
		-lsRegle: liste de règles.
	But:
		Afficher l'ensemble des règles contenues dans lsRegle en les numérotant.
	Sortie:
		Aucune.
*/
void lrAfficher(l_regle lsRegle);

/*
	Entrée:
		-lsRegle: liste de regles.
		-num: Numéro de la regle.
	But:
		Extraire et retourner la regle placée à la position num dans la liste lsRegle.
	Sortie:
		Règle extraite.
*/
l_regle regleParNum(l_regle lsRegle, int num);

/*
	Entrée:
		-lsRegle: liste de règles.
	But:
		Faire une vraie copie de lsRegle.
	Sortie:
		Liste de règles copie de lsRegle.
*/
l_regle  copieLRegle(l_regle lsRegle);

/*
	Entrée:
		-lsRegle: liste de regles.
	But:
		Supprimer la première règle de la liste de règles lsRegle.
	Sortie:
		Nouvelle lsRegle dont on a supprimé la tête.
*/
l_regle rSupprimerT(l_regle lsRegle);

/**
	Entrée:
		-lsRegle: liste de regles.
		-num: entier, numéro d'une règle.
	But:
		Supprimer définitivement la règle de numéro num de lsRegle.
	Sortie:
		lsRegle sans la règle numéro num.
**/
l_regle rSupprimerNum(l_regle lsRegle, int num);

/*
	Entrée:
		-lsRegle: liste de règles.
	But:
		Supprimer l'intégralité de lsRegle.
	Sortie:
		Aucune;
*/
void supprimerLRegle(l_regle lsregle);

/**
	Entrée:
	 -nFichier: Chaine de caractères.
	 -lsRegle: liste de règles.
	But:
		Sauvegarder l'intégralité des règles dans un fichier nFichier.
	Sortie:
		Aucune.
**/
void ecrireRegles(l_regle lsRegle, char* nFichier);

/**
	Entrée:
	 -nFichier: Chaine de caractères.
	But:
		Lire et mettre dans la mémoire du programme l'intégralité des règles sauvegardées dans le fichier nFichier.
	Sortie:
		lsRegle: liste de règles.
**/
l_regle lireRegles(char* nFichier);
