/**
	LO44
	Rémi Cabezas
	Fichier d'en-têtes des sous-programmes de fonctionnement du moteur d'inférence.
**/

#include "regles.h"

/*
	Entrée:
		-lsProp: liste de propositions.
		-prop: chaine de caractères dont on veut tester l'appartenance.
    -cpt: compteur de propositions, à initialiser à 1.
	But:
		Tester si une proposition prop appartient à lsProp.
	Sortie:
		- 0 si la chaine de caractère n'appartient pas à la liste.
		- le numéro de la proposition sinon.
*/
int appartient(l_prop lsProp, char* prop, int cpt);

/*
	Entrée:
		-lsProp: liste de faits.
		-lsRegle: liste de règles
	But:
		Retourne toutes les conclusions déductibles des règles en fonction des conditions de lsProp.
	Sortie:
		liste des conclusions diponibles.
*/
l_prop moteur(l_prop lsProp, l_regle lsRegle);
