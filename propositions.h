/**
	LO44
	Rémi Cabezas
	Fichier d'en-têtes des sous-programmes de gestion des popositions.
**/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_SIZE 40 /**Taille max de la chaine de caractères.*/

typedef struct prop{
	char* valeur;
	struct prop *suivant;
}proposition;

typedef proposition *l_prop;

/*
	Entrée:
		-lsProp: liste de propositions.
		-libProp: libellé de la proposition.
	But:
		Créer une nouvelle proposition ayant pour valeur le libellé libProp et l'ajoute en tête de lsProp.
	Sortie:
		Nouvelle lsProp augmentée d'une proposition.
*/
l_prop pAjouterT(l_prop lsProp, char* libProp);

/*
	Entrée:
		-lsProp: liste de propositions.
		-libProp: libellé de la proposition.
	But:
		Créer une nouvelle proposition ayant pour valeur le libellé libProp et l'ajoute en queue de lsProp.
	Sortie:
		Nouvelle lsProp augmentée d'une proposition.
*/
l_prop pAjouterQ(l_prop lsProp, char* libProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Tester si la liste est vide ou non.
	Sortie:
		- 1 si la liste est vide.
		- 0 sinon.
*/
int lvide(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Accéder à la tête de la liste.
	Sortie:
		Proposition de tête.
*/
char* lTete(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Afficher la proposition en tête de lsProp.
	Sortie:
		Aucune.
*/
void pAfficher(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Afficher l'ensemble des propositions contenues dans lsProp en les numérotant.
	Sortie:
		Aucune.
*/
void lpAfficher(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Faire une vraie copie de lsProp.
	Sortie:
		Liste de propositions copie de lsProp.
*/
l_prop  copieLProp(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Compter le nombre de propositions de la liste.
	Sortie:
		Entier représentant le nombre de propositions.
*/
int nbProp(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Supprimer l'intégralité de lsProp.
	Sortie:
		Aucune;
*/
void supprimerLProp(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
		-num: Numéro de la proposition.
	But:
		Extraire et retourner la proposition placée à la position num dans la liste lsProp.
	Sortie:
		Proposition extraite.
*/
l_prop propParNum(l_prop lsProp, int num);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Supprimer la première proposition de la liste de proposition lsProp.
	Sortie:
		Nouvelle lsProp dont on a supprimé la tête.
*/
l_prop supprimerT(l_prop lsProp);

/*
	Entrée:
		-lsProp: liste de propositions.
		-num: Numéro de la proposition.
	But:
		Supprimer la proposition placée en position num dans lsProp;
	Sortie:
		lsProp sans l'élément numéro num.
*/
l_prop supprimerNum(l_prop lsProp, int num);

/*
	Entrée:
		-lsProp: liste de propositions.
	But:
		Supprimer le dernier élément de lsProp;
	Sortie:
		lsProp sans son dernier élément.
*/
l_prop supprimerQ(l_prop lsProp);

/**
	Entrée:
	 -nFichier: Chaine de caractères.
	 -lsProp: liste de règles.
	But:
		Sauvegarder l'intégralité des propositions dans un fichier nFichier.
	Sortie:
		Aucune.
**/
void ecrirePropositions(l_prop lsProp, char* nFichier);

/**
	Entrée:
	 -nFichier: Chaine de caractères.
	But:
		Lire et mettre dans la mémoire du programme l'intégralité des propositions sauvegardées dans le fichier nFichier.
	Sortie:
		lsProp: liste de propositions.
**/
l_prop lirePropositions(char* nFichier);
