/**
	LO44
	Rémi Cabezas
	Fichier source contenant les sous-programmes de fonctionnement du moteur d'inférence.
**/

#include "moteur_inference.h"

int appartient(l_prop lsProp, char* prop, int cpt)
{
	/*printf("appartient\n");*/
	if(lvide(lsProp))
	{
		return 0;
	}
	else
	{
		if(!strcmp(lTete(lsProp), prop)) /* Compare la chaine contenue dans lsProp et celle passée en paramètres*/
		{
			/*printf("end triv appartient\n");*/
			return cpt;
		}
		else
		{
			/*printf("rec appartient\n");*/
			return appartient(lsProp->suivant, prop, cpt + 1);
		}
	}
}

l_prop moteur(l_prop lsProp, l_regle lsRegle)
{
	/*printf("moteur\n");*/
	l_regle pr = NULL;
	l_prop pp = copieLProp(lsProp);
	l_prop ccl = NULL;
	l_regle cpRegle = copieLRegle(lsRegle);
	int i = 0;
	int j = nbProp(lsProp);
	int k = 0;
	int change = 0;
	while(!lvide(pp))
	{
		pr = cpRegle;
		while(pr != NULL)
		{
			i = appartient(pr->valeur.premisse, pp->valeur, 1);
			if(i != 0)
			{
				pr->valeur.premisse = supprimerNum(pr->valeur.premisse, i);
				if(rvide(pr->valeur))
				{
					pp = pAjouterQ(pp, pr->valeur.conclusion->valeur);
					change = 1;
				}
			}
			pr = pr->suivant;
		}
		if(change == 0 && k >= j) /*Si on n'a rien pu déduire de plus avec ce fait et qu'on a déjà testé les faits initialement donnés alors on l'ajoute en conclusion.*/
		{
			ccl = pAjouterQ(ccl, pp->valeur);
		}
		change = 0;
		pp = supprimerT(pp);
		k++;
	}
	/*printf("end moteur\n");*/
	supprimerLRegle(cpRegle);
	return ccl;
}
