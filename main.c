/**
	LO44
	Rémi Cabezas
	Fichier principal avec interface utilisateur.
**/

#include "moteur_inference.h"

void clean(const char *buffer, FILE *fp);

int main()
{
	int choix = 15;
	int nb = 0;
	l_prop lsProp = NULL;
	l_regle lsRegle = NULL;
	l_prop cpProp;
	l_prop prem;
	l_prop ccl;
	char libProp[MAX_SIZE];

	while(choix != 0)
	{
		printf("\n\n=============== système expert ===============\n\n");

		printf("Choisissez l'action que vous voulez réaliser:\n");
		printf("1) Ajouter une proposition à la liste des propositions utilisables.\n");
		printf("2) Afficher les propositions.\n");
		printf("3) Créer une règle.\n");
		printf("4) Afficher les règles.\n");
		printf("5) Supprimer une proposition.\n");
		printf("6) Supprimer une règle.\n");
		printf("7) Utiliser le moteur d'inférence.\n");
		printf("8) Restaurer les jeux de données sauvegardés (fichiers propositions.txt et regles.txt).\n");
		printf("9) Sauvegarder les propositions et les règles dans un fichier.\n");
		printf("0) Quitter le programme.\n");
		printf("Numéro de l'action choisie: ");
		fgets(libProp, sizeof(libProp), stdin);
		clean(libProp, stdin);
		sscanf(libProp, "%d", &choix);

		switch(choix)
		{
			case 1:
				printf("Libellé de la proposition à ajouter (40 caractères max): ");
				fgets(libProp, sizeof(libProp), stdin);
				clean(libProp, stdin);
				lsProp = pAjouterQ(lsProp, libProp);
			break;
			case 2:
				printf("Liste des propositions disponibles:\n\n");
				lpAfficher(lsProp);
			break;
			case 3:
				cpProp = copieLProp(lsProp);
				prem = NULL;
				ccl = NULL;
				do{
					printf("Liste des propositions disponibles: \n\n");
					lpAfficher(cpProp);
					printf("\nChoisissez le numéro d'une proposition à ajouter en prémisse de votre règle (0 pour arrêter): ");
					fgets(libProp, sizeof(libProp), stdin);
					clean(libProp, stdin);
					sscanf(libProp, "%d", &choix);
					if (choix != 0)
					{
						prem = pAjouterQ(prem, propParNum(cpProp, choix)->valeur);
						cpProp = supprimerNum(cpProp, choix);
						nb = nbProp(cpProp);
					}
				}while(choix != 0 && nb > 1);
				printf("Vous allez maintenant choisir la conclusion de cette règle.\n");
				printf("Liste des propositions disponibles: \n\n");
				lpAfficher(cpProp);
				printf("\nChoisissez le numéro d'une proposition qui sera la conclusion de votre règle: ");
				fgets(libProp, sizeof(libProp), stdin);
				clean(libProp, stdin);
				sscanf(libProp, "%d", &choix);
				if (choix == 0)
				{
					printf("\nVeuillez entrer un nombre valide!\n");
					choix = 1;
				}
				ccl = pAjouterQ(ccl, propParNum(cpProp, choix)->valeur);
				lsRegle = rAjouterQ(lsRegle, creerRegle(prem, ccl));
				printf("\nVoici la liste de règles actualisées:\n\n");
				lrAfficher(lsRegle);
				supprimerLProp(cpProp);
			break;
			case 4:
				lrAfficher(lsRegle);
			break;
			case 5:
				printf("Liste des propositions disponibles: \n\n");
				lpAfficher(lsProp);
				printf("\nChoisissez le numéro d'une proposition à supprimer définitivement (0 pour annuler): ");
				fgets(libProp, sizeof(libProp), stdin);
				clean(libProp, stdin);
				sscanf(libProp, "%d", &choix);
				if (choix != 0)
				{
					lsProp = supprimerNum(lsProp, choix);
				}
				else
				{
					printf("Opération annulée!\n");
					choix = 15;
				}
			break;
			case 6:
				printf("Liste des règles disponibles:\n\n");
				lrAfficher(lsRegle);
				printf("\nChoisissez le numéro d'une règle à supprimer définitivement (0 pour annuler): ");
				fgets(libProp, sizeof(libProp), stdin);
				clean(libProp, stdin);
				sscanf(libProp, "%d", &choix);
				if (choix != 0)
				{
					lsRegle = rSupprimerNum(lsRegle, choix);
				}
				else
				{
					printf("Opération annulée!\n");
					choix = 15;
				}
			break;
			case 7:
				cpProp = copieLProp(lsProp);
				prem = NULL;
				ccl = NULL;
				printf("Veuillez d'abord choisir les symptômes dans la liste des propositions disponibles.\n\n");
				do{
					printf("Liste des propositions disponibles: \n\n");
					lpAfficher(cpProp);
					printf("\nChoisissez le numéro d'une proposition à ajouter à la liste des faits (0 pour arrêter): ");
					fgets(libProp, sizeof(libProp), stdin);
					clean(libProp, stdin);
					sscanf(libProp, "%d", &choix);
					if (choix != 0)
					{
						prem = pAjouterQ(prem, propParNum(cpProp, choix)->valeur);
						cpProp = supprimerNum(cpProp, choix);
					}
				}while(choix != 0);
				printf("Le moteur d'inférence va maintenant itérer et vous retourner les conclusions qu'il en a déduit\n\n");
				ccl = moteur(prem, lsRegle);
				printf("Conclusions déductibles:\n\n");
				lpAfficher(ccl);
				choix = 15;
			break;
			case 8:
				lsProp = lirePropositions("propositions.txt");
				lsRegle = lireRegles("regles.txt");
			break;
			case 9:
				printf("Nom du fichier de sauvegarde des propositions (40 caractères max): ");
				fgets(libProp, sizeof(libProp), stdin);
				clean(libProp, stdin);
				ecrirePropositions(lsProp, libProp);
				printf("Nom du fichier de sauvegarde des règles (40 caractères max): ");
				fgets(libProp, sizeof(libProp), stdin);
				clean(libProp, stdin);
				ecrireRegles(lsRegle, libProp);
			break;
		}
	}


	return EXIT_SUCCESS;
}

/*Fonction permettant de vides le tampon pour les saisies de chaînes de caractères*/
void clean(const char *buffer, FILE *fp)
{
    char *p = strchr(buffer,'\n');
    if (p != NULL)
		{
        *p = 0;
		}
    else
    {
        int c;
        while ((c = fgetc(fp)) != '\n' && c != EOF);
    }
}
